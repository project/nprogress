CONTENTS OF THIS FILE
=====================

 * Introduction
 * Information
 * Installation
 * Support
 * Maintainers

INTRODUCTION
============

Current Maintainer for 7.x-1.x: Alan Bondarchuk <imacoda@gmail.com>

The NProgress module is the best way to incorporate the NProgress.js plugin into
Drupal.

INFORMATION
===========

The NProgress.js (http://ricostacruz.com/nprogress/) provides slim progress bars
like on YouTube, Medium, etc.
Featuring realistic trickle animations to convince your users that something is
happening!

INSTALLATION
============

If using Drush, drush dl nprogress and then drush en nprogress otherwise unpack,
place and enable just like any other module.

SUPPORT
=======

Please use the issue queue for filing bugs with this module at
http://drupal.org/project/issues/nprogress

MAINTAINERS
=======

 * Alan Bondarchuk (fortis) - https://www.drupal.org/user/376960
