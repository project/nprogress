<?php

/**
 * @file
 * Drush integration for the NProgress module.
 */

/**
 * The NProgress library URLs.
 */
define('NPROGRESS_DOWNLOAD_URL', 'https://github.com/rstacruz/nprogress/archive/master.zip');

/**
 * Implements hook_drush_command().
 */
function nprogress_drush_command() {
  $items['nprogress-download'] = array(
    'aliases' => array('np-d'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'description' => dt('Downloads the required NProgress from Github.'),
    'arguments' => array(
      'path' => dt('Optional. The path to the download folder. If omitted, Drush will use the default location (sites/all/libraries/nprogress).'),
    ),
  );

  return $items;
}

/**
 * Command to the download NProgress library.
 */
function drush_nprogress_download() {
  $args = func_get_args();

  if (count($args) == 1) {
    $destination = $args[0];
  }
  else {
    $drupal_root = drush_get_context('DRUSH_DRUPAL_ROOT');
    $destination = $drupal_root . '/sites/all/libraries';
  }

  $filepath = $destination . '/nprogress-master.zip';
  $extracted_dir = $destination . '/nprogress-master';
  $library_path = $destination . '/nprogress';

  $result = drush_download_file(NPROGRESS_DOWNLOAD_URL, $filepath)
    && drush_tarball_extract($filepath, $destination, FALSE)
    && drush_move_dir($extracted_dir, $library_path, TRUE);

  if ($result) {
    $message = dt('NProgress was downloaded to !path.', array(
      '!path' => $library_path,
    ));
    drush_log($message, 'success');
  }
  else {
    $message = dt('Drush was unable to download NProgress to !path.', array(
      '!path' => $destination,
    ));
    drush_log($message, 'error');
  }

  return $result;
}

/**
 * Implements drush_MODULE_post_COMMAND().
 */
function drush_nprogress_post_pm_enable() {
  $modules = func_get_args();

  if (in_array('nprogress', $modules)) {
    drush_log('NProgress library could be downloaded with `drush np-d`');
  }
}
