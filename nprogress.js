/**
 * @file
 * Integration file for NProgress module.
 */

(function (Drupal, $) {
  'use strict';

  Drupal.behaviors.ajaxNProgress = {
    attach: function (context, settings) {
      if (typeof NProgress === 'undefined') {
        return;
      }

      /* global NProgress */
      $(document, context).ajaxStart(function () {
        NProgress.start();
      });

      $(document, context).ajaxComplete(function () {
        NProgress.done();
      });

      $(document, context).ajaxError(function () {
        NProgress.done();
      });
    }
  };

}(Drupal, jQuery));
